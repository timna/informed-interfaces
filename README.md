INFORMED INTERFACES

VIEWSHED
Indicates everywhere that a turbine would be visible to the participant if they were to stand in the click location

A*
Generates least cost paths (based on elevation) between nodes entered interactively to indicate where they would like to see new routes
